from django.urls import path, include
from django.conf.urls import url
from microlly_app import views

app_name = 'microlly_app'
urlpatterns = [
    #path('', views.index, name ='index'),
    url(r'^$', views.index, name='index'),
    #url(r'^signup/$', views.view_signup, name='signup'),
    #url(r'^login/$', views.view_login, name='login'),
    #url(r'^login/$', views.view_login, {'templates': 'login.html'}, name='login'),
    #url(r'^logout/$', views.view_logout, name='logout'),
    #url(r'^logout/$', views.view_logout, {'next_page': '/'}, name='logout')
    path('signup/', views.view_signup, name='signup'),
    path('login/', views.view_login, name='login'),
    path('logout/', views.view_logout, name='logout'),
    path('create/', views.creationPost, name='create'),
    path('list/', views.postList, name="list"),
    path('UserPost/<str:author>', views.UserPost, name='UserPost'),
    path('post/<slug:slug>/<int:pk>', views.detailsPost, name = 'details'),
    path('edit/<slug:slug>/<int:pk>', views.editPost, name='edit'),
    path('delete/<slug:slug>/<int:pk>', views.deletePost, name='delete'),
]
