from microlly_app.models import Publication
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import render, redirect, get_object_or_404
from microlly_app import forms
from microlly_app.forms import SignUpForm

# Create your views here.

def index(request):
    publications = Publication.objects.all()
    return render(request, 'index.html', {'latests_publications': publications})

def view_signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('microlly_app:index')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

def view_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('microlly_app:index')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})


def view_logout(request):
        logout(request)
        return redirect('microlly_app:index')

#//////////////////////////////////////////



def detailsPost (request, slug, pk):
    post = get_object_or_404(Publication, slug=slug, pk=pk)
    return render(request, 'details.html', {'post':post})

@login_required(login_url = "login/")
def creationPost(request):
    if request.method == 'POST':
        form = forms.PostCreate(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('microlly_app:index')
    else:
        form = forms.PostCreate()
    return render(request, 'creationPost.html', {'form':form})

@login_required(login_url = "login/")
def editPost(request, slug, pk):
    post = get_object_or_404(Publication, slug=slug, pk=pk)
    form = forms.PostEdit(request.POST or None, instance=post)
    if request.user != post.author:
        return redirect('microlly_app:index')
    if request.method =='POST':
        if form.is_valid():
            post.save()
            return redirect('microlly_app:index')
    context = {
        "form":form,
        "post":post,
    }
    return render(request, 'editPost.html', context)


def deletePost (request, slug, pk):
    post = get_object_or_404(Publication, pk=pk)
    if request.user != post.author:
        return redirect('microlly_app:index')
        #return PermissionDenied
    if request.method == 'POST':
        post.delete()
        return redirect('microlly_app:index')

def postList(request):
    posts = Publication.objects.all().order_by('creation_date')
    return render(request, 'list.html', { 'posts': posts })


def UserPost(request, author):
        publication_list = Publication.objects.filter(author=author)
        publication = paginator.get_page(page)
        return render(request, 'UserPost.html', context)
        redirect('microlly_app:index')
