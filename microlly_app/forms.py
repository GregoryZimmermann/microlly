from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from microlly_app import models

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(label='Prenom',max_length=30, required=False, help_text='Optionelle.')
    last_name = forms.CharField(label='Nom',max_length=30, required=False, help_text='Optionelle.')
    email = forms.EmailField(max_length=254, required=True,)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )

class CustomAuthenticationForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        if not user.is_active or not user.is_validated:
            raise forms.ValidationError('Probleme d identifiant.', code='invalid_login')


class PostCreate(forms.ModelForm):
        class Meta:
            model = models.Publication
            fields = ['title', 'body']

class PostEdit(forms.ModelForm):
        class Meta:
            model = models.Publication
            fields = ['title', 'body']
