from django.contrib import admin
from microlly_app.models import *

# Register your models here.
class PublicationAdmin(admin.ModelAdmin):
    list_display = ('title', 'creation_date', )
    search_fields = ('title', )
    
#Prendre en compte le paramétrage, en déclarant model comme candidat
admin.site.register(Publication, PublicationAdmin)    